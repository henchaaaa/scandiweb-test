<?php 

  $dbname = "localhost";
  $dbuser = "root";
  $dbpass = "password";

  // Create connection
  $conn = new mysqli($dbname, $dbuser, $dbpass);
  echo "Connection succesful";
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  }

  
  // Create database
  $sql = "CREATE DATABASE Store";
  if ($conn->query($sql) === TRUE) {
      echo "Database created successfully";
  } else {
      echo "Error creating database: " . $conn->error;
  }

  // Create table
  $sql = "CREATE TABLE products (
    id INT(10) AUTO_INCREMENT NOT NULL PRIMARY KEY,
    SKU BINARY(10) NOT NULL UNIQUE,
    product_name CHAR(255)  NOT NULL,
    product_price DECIMAL(10) NOT NULL,
    product_size VARCHAR(10), 
    product_weight DECIMAL(10),
    product_height  DECIMAL(10),
    product_width  DECIMAL(10),
    product_height  DECIMAL(10)
  )";

  $conn->close();
?> 